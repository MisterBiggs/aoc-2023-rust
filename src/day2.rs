// use itertools::Itertools;
use std::cmp;
use std::fs;

pub fn run() {
    println!("Day 2:");
    let input = fs::read_to_string("./inputs/day2.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

#[derive(Debug)]
struct Game {
    red: usize,
    green: usize,
    blue: usize,
}

impl Game {
    fn contains(&self, other: Game) -> bool {
        other.red <= self.red && other.green <= self.green && other.blue <= self.blue
    }
}

fn part1(games: &str) -> usize {
    let mut valid_games: Vec<usize> = vec![];

    let main_game = Game {
        red: 12,
        green: 13,
        blue: 14,
    };

    for game in games.lines() {
        let (game_part, rounds) = game.split_once(':').unwrap();
        let game_number = game_part
            .split_once(' ')
            .unwrap()
            .1
            .parse::<usize>()
            .unwrap();

        let mut valid_round: Vec<bool> = vec![];
        for round in rounds.split(';') {
            let mut round_counts = Game {
                red: 0,
                green: 0,
                blue: 0,
            };
            let dice = round.split(',');
            for die in dice {
                let (amount, color) = die.trim_start().split_once(' ').unwrap();

                match color {
                    "red" => round_counts.red = amount.parse().unwrap(),
                    "green" => round_counts.green = amount.parse().unwrap(),
                    "blue" => round_counts.blue = amount.parse().unwrap(),
                    _ => panic!("No color match found"),
                }
            }
            valid_round.push(main_game.contains(round_counts));
        }
        if valid_round.iter().all(|x| x.to_owned()) {
            valid_games.push(game_number);
        }
    }
    valid_games.into_iter().sum()
}

fn part2(games: &str) -> usize {
    let mut game_powers: Vec<usize> = vec![];
    for game in games.lines() {
        let (_, rounds) = game.split_once(':').unwrap();

        let mut game_minimums = Game {
            red: 0,
            green: 0,
            blue: 0,
        };
        for round in rounds.split(';') {
            let dice = round.split(',');
            for die in dice {
                let (amount_str, color) = die.trim_start().split_once(' ').unwrap();
                let amount = amount_str.parse::<usize>().unwrap();

                match color {
                    "red" => game_minimums.red = cmp::max(game_minimums.red, amount),
                    "green" => game_minimums.green = cmp::max(game_minimums.green, amount),
                    "blue" => game_minimums.blue = cmp::max(game_minimums.blue, amount),
                    _ => panic!("No color match found"),
                }
            }
        }

        game_powers.push(game_minimums.red * game_minimums.green * game_minimums.blue);
    }
    game_powers.into_iter().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

        assert_eq!(part1(input), 8);
    }

    #[test]
    fn test_2() {
        let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

        assert_eq!(part2(input), 2286);
    }
}
