use std::collections::HashSet;
use std::fs;

use itertools::Itertools;

pub fn run() {
    println!("Day 4:");
    let input = fs::read_to_string("./inputs/day4.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(cards: &str) -> usize {
    let mut total_wins = 0;
    for card in cards.lines() {
        let (_, card_numbers) = card.split_once(':').unwrap();
        let (winners_raw, chances_raw) = card_numbers.split_once('|').unwrap();

        let winners = winners_raw
            .split(' ')
            .filter(|s| !s.is_empty())
            .map(|win| win.trim().parse::<usize>().unwrap())
            .collect::<HashSet<usize>>();
        let chances = chances_raw
            .split(' ')
            .filter(|s| !s.is_empty())
            .map(|chance| chance.trim().parse::<usize>().unwrap())
            .collect::<HashSet<usize>>();

        let wins = winners.intersection(&chances).count();
        if wins != 0 {
            total_wins += usize::pow(2, wins.saturating_sub(1) as u32);
        }
    }

    total_wins
}

fn part2(cards: &str) -> usize {
    let number_of_cards: usize = cards.lines().collect::<Vec<_>>().len();
    let mut card_wins = vec![0; number_of_cards];

    for (current_card_number, card) in cards.lines().enumerate() {
        let (_, card_numbers) = card.split_once(':').unwrap();
        let (winners_raw, chances_raw) = card_numbers.split_once('|').unwrap();

        let winners = winners_raw
            .split(' ')
            .filter(|s| !s.is_empty())
            .map(|win| win.trim().parse::<usize>().unwrap())
            .collect::<HashSet<usize>>();
        let chances = chances_raw
            .split(' ')
            .filter(|s| !s.is_empty())
            .map(|chance| chance.trim().parse::<usize>().unwrap())
            .collect::<HashSet<usize>>();

        let wins = winners.intersection(&chances).count();

        for index in 1..=wins {
            card_wins[index + current_card_number] += 1 + card_wins[current_card_number];
        }
    }

    card_wins.iter().sum::<usize>() + number_of_cards
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

        assert_eq!(part1(input), 13);
    }

    #[test]
    fn test_2() {
        let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

        assert_eq!(part2(input), 30);
    }
}
