use std::{fs, vec};

use itertools::Itertools;
use regex::Regex;

pub fn run() {
    println!("Day 3:");
    let input = fs::read_to_string("./inputs/day3.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(schematic: &str) -> usize {
    let mut sum_of_parts = 0;
    let padded_schematic = format!(".\n{schematic}\n.");
    let re = Regex::new(r"\d+").unwrap();
    let windows = padded_schematic
        .lines()
        .tuple_windows::<(_, _, _)>()
        .collect::<Vec<_>>();
    for (top, middle, bottom) in windows {
        for number in re.find_iter(middle) {
            let search_range = std::ops::Range {
                start: number.range().start.saturating_sub(1),
                end: number.range().end + 1,
            };

            let mut search_values: Vec<char> = vec![];
            for index in search_range {
                search_values.push(top.chars().nth(index).unwrap_or('.'));
                search_values.push(middle.chars().nth(index).unwrap_or('.'));
                search_values.push(bottom.chars().nth(index).unwrap_or('.'));
            }
            if search_values
                .into_iter()
                .filter(|c| !c.is_ascii_digit())
                .filter(|c| c != &'.')
                .count()
                != 0
            {
                sum_of_parts += number.as_str().parse::<usize>().unwrap();
            }
        }
    }
    sum_of_parts
}

fn part2(schematic: &str) -> usize {
    let mut sum_of_parts = 0;
    let padded_schematic = format!(".\n{schematic}\n.");
    let number_re = Regex::new(r"\d+").unwrap();
    let gear_re = Regex::new(r"\*").unwrap();
    let windows = padded_schematic
        .lines()
        .tuple_windows::<(_, _, _)>()
        .collect::<Vec<_>>();

    for (top, middle, bottom) in windows {
        for gear in gear_re.find_iter(middle) {
            let search_range = std::ops::Range {
                start: gear.range().end.saturating_sub(1),
                end: gear.range().end + 1,
            };

            let mut numbers = vec![];
            for row in [top, middle, bottom] {
                for num_match in number_re.find_iter(row) {
                    numbers.push(num_match);
                }
            }

            let mut geared_numbers = vec![];
            for number in numbers {
                if std::cmp::max(number.range().start + 1, search_range.start)
                    <= std::cmp::min(number.range().end, search_range.end)
                {
                    geared_numbers.push(number.as_str().parse::<usize>().unwrap())
                }
            }

            if geared_numbers.len() == 2 {
                sum_of_parts += geared_numbers[0] * geared_numbers[1];
            }
        }
    }
    sum_of_parts
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

        assert_eq!(part1(input), 4361);
    }

    #[test]
    fn test_2() {
        let input = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

        assert_eq!(part2(input), 467835);
    }
}
